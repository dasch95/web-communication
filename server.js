const express = require('express');
const path = require('path');
const fs = require('fs');
const http = require('http');
const https = require('https');
const sio = require('socket.io');
const favicon = require('serve-favicon');
const compression = require('compression');

const app = express(),
  options = { 
    key: fs.readFileSync(__dirname + '/localhost-key.pem'),
    cert: fs.readFileSync(__dirname + '/localhost-cert.pem')
  },
  port = process.env.PORT || 5000,
  server = process.env.NODE_ENV === 'production' ?
    http.createServer(app).listen(port) :
    https.createServer(options, app).listen(port),
  io = sio(server);
// kompresja żądań 
app.use(compression());
app.use(express.static(path.join(__dirname, 'dist')));
app.use((req, res) => res.sendFile(__dirname + '/dist/index.html'));
app.use(favicon('./dist/favicon.ico'));
// Wyłącz domyslny nagłowek'X-Powered-By: Express' 
app.disable('x-powered-by');
io.sockets.on('connection', socket => {
  let room = '';
  const create = err => {
    if (err) {
      return console.log(err);
    }
    socket.join(room);
    socket.emit('create');
  };
  //wyslij wiadomość do klienta nie do nadawcy
  socket.on('message', message => socket.broadcast.to(room).emit('message', message));
  socket.on('find', () => {
    const url = socket.request.headers.referer.split('/');
    room = url[url.length - 1];
    const sr = io.sockets.adapter.rooms[room];
    if (sr === undefined) {
      // nie ma pokoju o takiej nazwie, tworzy się nowy.
      socket.join(room);
      socket.emit('create');
    } else if (sr.length === 1) {
      socket.emit('join');
    } else { // maksymalnie 2 osoby (P2P)
      socket.emit('full', room);
    }
  });
  socket.on('auth', data => {
    data.sid = socket.id;
    // wyślij wiadomość do klienta nie do serwera
    socket.broadcast.to(room).emit('approve', data);
  });
  socket.on('accept', id => {
    io.sockets.connected[id].join(room);
    //wyślij akceptację połączenia (potwierdzenie) do wszystkich wraz z nadawcą( pełnioncy rolę serwera)
    io.in(room).emit('bridge');
  });
  socket.on('reject', () => socket.emit('full'));
  socket.on('leave', () => {
    // emituj do wszystkich poza pelniacym role serwera/nadawcy
    socket.broadcast.to(room).emit('hangup');
    socket.leave(room);});
});

