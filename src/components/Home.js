import React from 'react';
import { PropTypes } from 'prop-types';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';

//<Link className="primary-button" to={ '/TwojPokoj/' + props.defaultRoomId }>Losuj</Link>
const Home = props =>
  <div className="home">
    <div>
      <h1 itemProp="headline">Witaj w chat-projekt PT!</h1>
      <p>Wpisz nazwę pokoju</p>
      <input type="text" name="room" value={ props.roomId } onChange={props.handleChange} pattern="^\w+$" maxLength="10" required autoFocus title="Nazwa musi posiadać liczby bądz litery!."/>
      <Link className="primary-button" to={ '/TwojPokoj/' + props.roomId }>Dołącz</Link>
    </div>
  </div>;

Home.propTypes = {
  handleChange: PropTypes.func.isRequired,
  defaultRoomId: PropTypes.string.isRequired,
  roomId: PropTypes.string.isRequired,
  rooms: PropTypes.array.isRequired
};

const mapStateToProps = store => ({rooms: store.rooms});

export default connect(mapStateToProps)(Home);