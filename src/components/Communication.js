import { PropTypes } from 'prop-types';
import React from 'react';
import { Link } from 'react-router-dom';

import ToggleFullScreen from './ToggleFullScreen';

const Communication = props =>
  <div className="auth">
    <div className="media-controls">
      <button onClick={props.toggleAudio} className={'audio-button-' + props.audio}>
          <img src="http://files.softicons.com/download/business-icons/pretty-office-icons-by-custom-icon-design/png/256/delete.png" width="40em" height="40em"/> 
      </button>
      <button onClick={props.toggleVideo} className={'video-button-' + props.video}>
      <img src="http://icons.iconarchive.com/icons/webalys/kameleon.pics/512/Video-Camera-2-icon.png" width="40em" height="40em"/> 
      </button>
      <button onClick={ToggleFullScreen} className="fullscreen-button">
      <img src="https://image.flaticon.com/icons/png/512/217/217226.png" width="40em" height="40em"/> 
      </button>
    </div>
    <div className="request-access">
      <p><span className="you-left">Dołączyłeś.&nbsp;</span>Przywitaj się z rozmowca.</p>
      <form onSubmit={props.send}>
        <input type="text" autoFocus onChange={props.handleInput} data-ref="message"  maxLength="30" required placeholder="Przywitaj się..." />
        <button className="primary-button">Wyślij</button>
      </form>
    </div>
    <div className="grant-access">
      <p>Ktos chce porozmawiać... </p>
      <div dangerouslySetInnerHTML={props.getContent(props.message)}></div>
      <button onClick={props.handleInvitation} data-ref="reject" className="primary-button">NIE</button>
      <button onClick={props.handleInvitation} data-ref="accept" className="primary-button">OK</button>
    </div>
    <div className="room-occupied">
      <p>Sprobuj w innym pokoju - Osiągnięto limit miejsc</p>
      <Link  className="primary-button" to="/">OK</Link>
    </div>
    <div className="waiting">
      <p><span>Poczekaj na połączenie.&nbsp;</span>
      <span className="remote-left">User się rozłączył</span></p>
    </div>
  </div>

Communication.propTypes = {
  message: PropTypes.string.isRequired,
  audio: PropTypes.bool.isRequired,
  video: PropTypes.bool.isRequired,
  toggleVideo: PropTypes.func.isRequired,
  toggleAudio: PropTypes.func.isRequired,
  getContent: PropTypes.func.isRequired,
  send: PropTypes.func.isRequired,
  handleHangup: PropTypes.func.isRequired,
  handleInput: PropTypes.func.isRequired,
  handleInvitation: PropTypes.func.isRequired
};

export default Communication;
